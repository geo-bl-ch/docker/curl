# Changelog

## v1.3.1

- package "coreutils" added

## v1.3.0

- alpine:3.20

## v1.2.0

- alpine:3.19

## v1.1.0

- alpine:3.16

## v1.0.1

- Change default command to "curl --help"

## v1.0.0

- Initial version
