FROM alpine:3.20

RUN apk update && \
    apk upgrade && \
    apk add \
        bash \
        curl \
        jq \
        coreutils && \
    mkdir /app && \
    adduser -S -u 1001 -G root -h /app curl && \
    chown -R 1001:0 /app && \
    chmod -R g=u /app && \
    chgrp 0 /etc/passwd && \
    chmod g=u /etc/passwd

COPY --chown=1001:0 uid_entrypoint.sh /usr/local/bin/

WORKDIR /app

ENTRYPOINT ["/usr/local/bin/uid_entrypoint.sh"]

CMD ["curl", "--help"]

USER 1001
